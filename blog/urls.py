from django.conf.urls import url, include, handler404, handler500
from django.contrib import admin
from django.urls import path
from home.views import *
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView
from . import views

admin.site.site_header = 'Lisansüstü Eğitim Enstitüsü - Yönetim Sayfası'

urlpatterns = [

    url(r'^$', home_view, name='home'),

    url(r'^logout/$', logout_view, name="logout"),

    url(r'^graph/', include('graph.urls')),

    url(r'^admin/', admin.site.urls),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#handler404 = views.error_404
#handler500 = views.error_500

