from django.shortcuts import render, HttpResponse, get_object_or_404, HttpResponseRedirect, redirect, \
    Http404


def error_404(request, exception):
    return render(request, 'post/404.html', status=404)

def error_500(request, exception):
    return render(request, 'post/404.html', status=500)