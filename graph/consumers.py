from typing import Any, Union

from django.contrib.auth import get_user_model
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json

from django.shortcuts import get_object_or_404
from django.utils import formats
from django.db.models.functions import Now
from datetime import datetime

from django.db import IntegrityError
from django.db import connection
import logging

User = get_user_model()

class Consumer(WebsocketConsumer):


    def connect(self):
    #    self.test_id = self.scope['url_route']['kwargs']['test_id']
        self.user = self.scope['user']

        self.room_group_name = 'process'
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
    #    self.test_id = self.scope['url_route']['kwargs']['test_id']
        self.user = self.scope['user']

        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive(self, text_data):
        data = json.loads(text_data)
        self.commands[data['command']](self, data)

    def send_message_to_yourself(self, message):  #içeriden gelen mesaj (kendi kendine)
        self.send(text_data=json.dumps(message))

    def send_message_to_all_clients(self, message):
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'send_all_clients_message',
                'message': message
            }
        )

    def send_all_clients_message(self, event):  #send_chat_message prosedürü ile dışarıdan gelen mesaj pear-to-pear
        message = event['message']
        self.send(text_data=json.dumps(message))


