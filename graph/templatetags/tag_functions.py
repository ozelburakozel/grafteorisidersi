from django import template
from django.shortcuts import get_object_or_404, redirect
from datetime import datetime, timezone
from django.db.models.functions import StrIndex
from django.urls import reverse
from django.utils import formats
from django.utils.dateformat import DateFormat
from django.db.models import Max
import logging

register = template.Library()

@register.filter(name='getdate')
def getdate(value):
    return DateFormat(datetime.now()).format('d/m/Y')

@register.filter(name='getguid')
def getguid(value):
    tmp = "3046d665-bccb-4637-bf95-f9dbc0728981"
    return tmp[24:]
    #uuid.uuid4()


@register.filter(name='getvideotype')
def getvideotype(value):
    n = value.find('https')
    return n