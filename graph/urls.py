from django.conf.urls import url
from .views import *
from django.views.generic import TemplateView

app_name = "graph"

urlpatterns = [
    url(r'^graph_index/$', graph_index, name='graph_index'),
]