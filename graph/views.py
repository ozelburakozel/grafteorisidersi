from django.shortcuts import render, HttpResponse, get_object_or_404, HttpResponseRedirect, redirect, Http404
from django.contrib.auth.decorators import login_required
from django.utils.safestring import mark_safe
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings as conf_settings
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from .forms import *
from .models import *
import logging
import json

import networkx as nx
import numpy as np
from itertools import combinations
from networkx.drawing.nx_pydot import write_dot

from itertools import combinations
def grafkombinasyon(tekdereceliler):
    kombs = list(combinations(tekdereceliler, 2)) #2li kombinasyonları buluyoruz

    # 2li kombinasyonlardan hiç bir eleman dışarıda kalmayacak tüm kombinasyonları buluyoruz.
    k1 = int(len(tekdereceliler)/2)
    k2 = len(kombs)
    kombs_v2 = []
    for n in range(k1, k2+1):
        v2list = list(combinations(kombs, n))
        for aa in v2list:
            kombs_v2.append(aa)

    kombs_v3 = []
    for i in range(len(kombs_v2)):
        temp = []
        for j in range(len(kombs_v2[i])):
            for z in range(len(kombs_v2[i][j])):
                temp.append(kombs_v2[i][j][z])

        ekle = True
        for td in tekdereceliler:
            if temp.count(td) % 2 == 0:
                ekle = False

        if ekle:
            kombs_v3.append(kombs_v2[i])

    return kombs_v3


def graph_index(request):
    colors = ['red', 'green', 'blue', 'orange', 'yellow', 'purple']
    komsuluk_matrisi = []
    with open('komsuluk2.csv') as f:
        for satir in f.readlines():
            komsuluk_matrisi.append(list(map(int, satir.split(','))))

    #Çoklu graph oluştur
    G1 = nx.MultiGraph()

    #Dosyadan Oku
    A = np.array(komsuluk_matrisi)
    G1 = nx.from_numpy_array(A, create_using=nx.MultiGraph)
    logging.warning("İlk Hali Eulerian MI=%s" % nx.is_eulerian(G1))
    baslangicmaliyet = G1.size(weight="weight")
    logging.warning("İlk Hali Toplam Maliyet=%s" % baslangicmaliyet)

    #Tek dereceli düğümleri bul
    tekdereceliler = []
    for n in G1.nodes:
        if G1.degree[n] % 2 == 1:
            tekdereceliler.append(n)

    logging.warning("Tek Dereceli Tepeler= %s" % tekdereceliler)

    s = ''
    for g in G1.edges():
        s = s + str(g[0])+' -- '+str(g[1])+';'

    logging.warning(s)

    nodes = []
    for g in G1.nodes():
        nodes.append({
            "id": g,
            "label": str(g),
            "shape": 'circle',
            "color": 'blue'
        })

    font_edges_yellow = ({
        "background": 'yellow'
    })

    font_edges_red = ({
        "background": 'red'
    })

    edges = []
    for g in G1.edges.data():
        edges.append({
            "from": g[0],
            "to": g[1],
            "color": 'black',
        #    "value": g[2]['weight'],
            "value": 2,
            "label": g[2]['weight'],
            "font": font_edges_yellow,
        })

    #Kombinasyonları oluştur
    kombs = list(grafkombinasyon(tekdereceliler))

    min_maliyet = -1
    min_index = -1
    for i in range(len(kombs)):
        top = 0
        for j in range(len(kombs[i])):
            sp = nx.shortest_path_length(G1, source=kombs[i][j][0], target=kombs[i][j][1], weight='weight')
    #        print("(%s,%s)->%s" % (kombs[i][j][0],kombs[i][j][1],sp))
            top = top + sp
    #    print("%s-Maliyet->%s" % (kombs[i],top))
        if min_maliyet == -1:
            min_maliyet = top
            min_index = i
        else:
            if top<min_maliyet:
                min_maliyet=top
                min_index = i

    logging.warning("\nMinimum Maliyet=%s" % min_maliyet)
    logging.warning("Minimum İndex=%s" % min_index)
    logging.warning(kombs[min_index])

    # Minimum Yol
    yollar = []
    H = G1.copy()
    for j in range(len(kombs[min_index])):
        yol = nx.shortest_path(H, source=kombs[min_index][j][0], target=kombs[min_index][j][1], weight='weight')
        yollar.append(yol)

    newedges = []
    for i in range(len(yollar)):
        path_edges = zip(yollar[i], yollar[i][1:])

        for e in path_edges:
            logging.warning("Yol->%s,%s" % ( e[0], e[1]))
            H.add_edge(e[0], e[1], label=H[e[0]][e[1]][0]['weight'], weight=H[e[0]][e[1]][0]['weight'])
            newedges.append({
                "from": e[0],
                "to": e[1],
                "color": colors[i % 6],
            #    "value": H[e[0]][e[1]][0]['weight'],
                "value": 2,
                "label": H[e[0]][e[1]][0]['weight'],
                "font": font_edges_yellow,
            })

    logging.warning("Son Hali Eulerian MI=%s" % nx.is_eulerian(H))
    logging.warning("Son Hali Toplam Maliyet=%s" % H.size(weight="weight"))

    # END - Minimum Yol

    logging.warning("Tüm Çözüm Sayısı=%s" % len(kombs))
    # Tüm Olası Yolllar
    tumcozumler = []

    for k in range(len(kombs)):
        logging.warning("************ Çözüm-%s ************" % k)
        logging.warning(kombs[k])
        H = G1.copy()
        yollar = []
        for j in range(len(kombs[k])):
            yol = nx.shortest_path(H, source=kombs[k][j][0], target=kombs[k][j][1], weight='weight')
            yollar.append(yol)

        newedges_temp = []

        s = ''
        ym = 0
        for i in range(len(yollar)):
            path_edges = zip(yollar[i], yollar[i][1:])
            logging.warning("len=%s" % len(yollar[i][1:]))
            sub_s = ''
            u = 0
            for e in path_edges:
                logging.warning("Yol->%s,%s YolNo:%s-LineNo:%s-Color:%s" % (e[0], e[1], i, u, colors[i % 6]))
                H.add_edge(e[0], e[1], label=H[e[0]][e[1]][0]['weight'], weight=H[e[0]][e[1]][0]['weight'])
                newedges_temp.append({
                    "from": e[0],
                    "to": e[1],
                    "color": colors[i % 6],
                    #    "value": H[e[0]][e[1]][0]['weight'],
                    "value": 2,
                    "label": H[e[0]][e[1]][0]['weight'],
                    "font": font_edges_yellow,
                })
                sub_s = '(' + str(e[0])+','+str(e[1])+')'

                if u == len(yollar[i][1:])-1:
                    logging.warning("u == len")
                    sub_s = sub_s + '-'
                s = s + sub_s
                ym = ym + H[e[0]][e[1]][0]['weight']
                u += 1

        tumcozumler.append({
            "newedges": newedges_temp,
            "maliyet": H.size(weight="weight"),
            "eulerian": nx.is_eulerian(H),
            "yol": s[:-1],
            "adi": "%s --> Toplam Maliyet:%s" % (kombs[k], H.size(weight="weight"))
        })

        logging.warning("Son Hali Eulerian MI=%s" % nx.is_eulerian(H))
        logging.warning("Son Hali Toplam Maliyet=%s" % H.size(weight="weight"))

    # END - Tüm Olası Yolllar

    context = {
        'cozum': mark_safe(json.dumps('1 -- 2; 2 -- 3; 2 -- 4; 2 -- 1; 2 -- 1; 3 -- 2; 4 -- 2')),
        'graf': mark_safe(json.dumps(s)),
        'nodes': mark_safe(json.dumps(nodes)),
        'edges': mark_safe(json.dumps(edges)),
        'newedges': mark_safe(json.dumps(newedges)),
        'tumcozumler': mark_safe(json.dumps(tumcozumler)),
        'tekdereceliler': "%s" % tekdereceliler
    }
    return render(request, "cpp.html", context)