from django.shortcuts import render, HttpResponse, get_object_or_404, HttpResponseRedirect, redirect, Http404
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib import messages
from django.db import connection
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.utils.safestring import mark_safe

import json
import logging

User = get_user_model()

def home_view(request):
    context = {

        'activeform': 'kullanici',
    }
    return render(request, "home.html", context)


def logout_view(request):
    logout(request)
    return redirect('home')


